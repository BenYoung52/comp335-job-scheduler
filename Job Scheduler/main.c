// Client side C/C++ program to demonstrate Socket programming
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#define PORT 8096

struct serverInfo {
    char type[1024];
    char id[32];
    char state[32];
    char time[32];
    char cores[32];
    char memory[32];
    char diskSpace[32];
};

struct job {
    char time[32];
    char id[32];
    char runtime[32];
    char cores[32];
    char memory[32];
    char disk[32];
};

struct serverInfo findLargest(int sock, struct job jobs) {
    char buffer[1024] = {0};
    char buffer2[1024] = {0};
    int finish = 1, valread;
    int largestCpu = 0;
    struct serverInfo largest;
    
    
    printf("start avail\n");
    strcpy(buffer, "RESC All");
    //sprintf(buffer, "RESC Avail %s %s %s", jobs.cores, jobs.memory, jobs.disk);
    printf("%s\n",buffer );
    send(sock , buffer , strlen(buffer) , 0 );
    
    //Recieve "DATA"
    strcpy(buffer, "");
    valread = read(sock, buffer2, 1024);
    printf("Recieved %s\n",buffer2 );
    
    if(strcmp(buffer2, "DATA") == 0) {
        strcpy(buffer, "OK");
        send(sock , buffer , strlen(buffer) , 0 );
        
        //Recieve first set of job data
        strcpy(buffer, "");
        valread = read( sock , buffer, 1024);
        printf("%s\n",buffer );
        
        
        while(finish) {
            char str[1024] = {0};
            struct serverInfo current;
            strcpy(str, buffer);
            
            //This assumes server will always return the same number of values returned from RESC. If not may cause problems
            char *token = strtok(str, " ");
            strcpy(current.type, token);
            printf("%s\n", token);
            
            token = strtok(NULL, " ");
            strcpy(current.id, token);
            printf("%s\n", token);
            
            token = strtok(NULL, " ");
            strcpy(current.state, token);
            printf("%s\n", token);
            
            token = strtok(NULL, " ");
            strcpy(current.time, token);
            printf("%s\n", token);
            
            token = strtok(NULL, " ");
            strcpy(current.cores, token);
            printf("%s\n", token);
            
            token = strtok(NULL, " ");
            strcpy(current.memory, token);
            printf("%s\n", token);
            
            token = strtok(NULL, " ");
            strcpy(current.diskSpace, token);
            printf("%s\n", token);
            
            int cpuCount = 0;
            
            
            cpuCount = atoi(current.cores);
            printf("----->%d\n", cpuCount);
            
            
            printf("%d, %d\n", cpuCount, largestCpu);
            
            if(cpuCount > largestCpu) {
                largest = current;
                largestCpu = cpuCount;
            }
            
            strcpy(buffer, "OK");
            send(sock , buffer , strlen(buffer) , 0 );
            valread = read( sock , buffer, 1024);
            printf("Last:%s\n",buffer );
            
            if(strcmp(buffer, ".K") == 0) {
                printf("finished");
                finish = 0;
            }
            
        }
        
        
    }
    return largest;
}

void schdLargest(struct serverInfo server, int sock, struct job jobs) {
    char buffer[1024] = {0};
    int valread;
    sprintf(buffer, "SCHD %s %s %s", jobs.id, server.type, "0");
    send(sock , buffer , strlen(buffer) , 0 );
    
    strcpy(buffer, " ");
    valread = read( sock , buffer, 1024);
    printf("%s\n",buffer );
}

struct job jobInfo(int sock, char buffer[]) {
    struct job jobs;
    
    char str[1024] = {0};
    strcpy(str, buffer);
    
    // Returns first token
    char *token = strtok(str, " ");
    
    token = strtok(NULL, " ");
    strcpy(jobs.time, token);
    printf("%s\n", token);
    
    token = strtok(NULL, " ");
    strcpy(jobs.id, token);
    printf("%s\n", token);
    
    
    token = strtok(NULL, " ");
    strcpy(jobs.runtime,token);
    printf("%s\n", token);
    
    
    token = strtok(NULL, " ");
    strcpy(jobs.cores, token);
    printf("%s\n", token);
    
    
    token = strtok(NULL, " ");
    strcpy(jobs.memory, token);
    printf("%s\n", token);
    
    
    token = strtok(NULL, " ");
    strcpy(jobs.disk, token);
    printf("%s\n", token);
    
    return jobs;
}

int main(int argc, char const *argv[])
{
    struct job jobs;
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};
    char largestServer[1024] = {0};
    struct serverInfo server;
    
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }
    
    memset(&serv_addr, '0', sizeof(serv_addr));
    
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    
    // Convert IPv4 and IPv6 addresses from text to binary form
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
    
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }
    
    strcpy(buffer, "HELO");
    send(sock , buffer , strlen(buffer) , 0 );
    printf("Hello message sent\n");
    
    valread = read( sock , buffer, 1024);
    printf("%s\n",buffer );
    
    strcpy(buffer, "AUTH comp335");
    send(sock , buffer , strlen(buffer) , 0 );
    
    strcpy(buffer, "");
    valread = read( sock , buffer, 1024);
    printf("%s\n",buffer );
    
    
    
    while(1) {
        
        
        char buffer[1024] = {0};
        
        strcpy(buffer, "REDY");
        send(sock , buffer , strlen(buffer) , 0 );
        
        printf("1st%s\n",buffer );
        valread = read( sock , buffer, 1024);
        printf("2nd%s\n",buffer );
        
        if(strcmp(buffer, "NONE") == 0) {
            break;
        }
        
        
        
        //JOBN
        printf("starting jobs\n");
        jobs = jobInfo(sock, buffer);
        printf("jobs, %s\n", jobs.disk );
        
        //Find available servers and return the largest server
        server = findLargest(sock, jobs);
        printf("%s\n","server" );
        
        //Schedule based on result from findLargest
        schdLargest(server, sock, jobs);
        printf("%s\n","schd" );
        
        
        //Check for 'NONE'. If 'NONE' leave loop, otherwise loop
        
    }
    strcpy(buffer, "QUIT");
    send(sock , buffer , strlen(buffer) , 0 );
    return 0;
    
}
