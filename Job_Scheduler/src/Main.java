import java.net.*; 
import java.io.*; 
import java.util.*; 
import org.w3c.dom.*;
import javax.xml.parsers.*;

public class Main 
{ 

	//Run the server with -n to make this work. e.g ./server -v -n brief
	//-n will tell the server to send messages with a new line character at the end so that readline() commands work

	public class serverInfo {
		String type, id, state, time, cores, memory, diskSpace;

		public serverInfo(String type, String id, String state, String time, String cores, String memory, String diskSpace) {
			this.type = type;
			this.id = id;
			this.state = state;
			this.time = time;
			this.cores = cores;
			this.memory = memory;
			this.diskSpace = diskSpace;
		}

	}

	public class job {
		String time, id, runtime, cores, memory, disk;

		public job(String time, String id, String runtime, String cores, String memory, String disk) {
			this.time = time;
			this.id = id;
			this.runtime = runtime;
			this.cores = cores;
			this.memory = memory;
			this.disk = disk;
		}
	}

	// initialize socket and input output streams 
	private Socket socket            = null; 
	private BufferedReader  input   = null; 
	private PrintWriter out     = null; 

	// constructor to put ip address and port 

	public Main(String address, int port, String args[]) 
	{ 
		// establish a connection 
		try
		{ 
			socket = new Socket(address, port); 
			System.out.println("Connected"); 

			// takes input from terminal 
			input  = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			// sends output to the socket 
			out    = new PrintWriter(socket.getOutputStream(), true); 
		} 
		catch(UnknownHostException u) 
		{ 
			System.out.println(u); 
		} 
		catch(IOException i) 
		{ 
			System.out.println(i); 
		} 

		// string to read message from input 
		String in = ""; 
		job jobs;
		serverInfo server = null;
		Map<String, Integer> sortedList = new LinkedHashMap<String, Integer>();


		try
		{
			out.println("HELO");
			in = input.readLine();
			System.out.println(in);


			out.println("AUTH ben"); //Change this to the name of your pc, comp335 for if running at uni computers
			in = input.readLine();
			System.out.println(in);


			sortedList = readXMLAndSort();

			while(true) {

				out.println("REDY");
				in = input.readLine();
				System.out.println(in);

				if(in.equals("NONE")) {
					break;
				}

				//Convert String input to job class
				jobs = jobInfo(in);

				switch(args[1]) {
				case "ff":
					server = firstFit(input, out, jobs, sortedList);
					break;
				case "bf":
					server = bestFit(input, out, jobs, sortedList);
					break;
				case "wf":
					server = worstFit(input, out, jobs, sortedList);
					break;
				case "cf":
					server = cheapestFit(input, out, jobs, sortedList);
				default:
					server = findLargest(input, out, jobs);

				} 
				//Just creates a SCHD request based on the job and chosen server
				schedule(input, out, jobs, server);

				

			}//while loop
			
			out.println("QUIT");
		}

		catch(IOException i) 
		{ 
			System.out.println(i); 
		} 


		// close the connection 
		try
		{ 
			input.close(); 
			out.close(); 
			socket.close(); 
		} 
		catch(IOException i) 
		{ 
			System.out.println(i); 
		} 
	}

	public static void main(String args[]) 
	{ 
		if(args.length > 1) {
			if(args[0].equals("-a") && (args[1].equals("ff") || args[1].equals("bf") || args[1].equals("wf"))) {	
			} else {
				System.out.println("Invalid arguments. Use -a followed by either ff, bf or wf");
				return;
			}
		}

		Main client = new Main("127.0.0.1", 8096, args); 

	} 

	public Map<String, Integer> readXMLAndSort() { //XML Parser, currently only for FirstFit
		String filePath = "system.xml";
		DocumentBuilder builder = null;
		Document document = null;
		Map<String, Integer> serverSizes = new LinkedHashMap<String, Integer>();
		Map<String, Integer> sortedList = new LinkedHashMap<String, Integer>();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			builder = factory.newDocumentBuilder();
			document = builder.parse(new File( filePath ));
			NodeList servers = document.getElementsByTagName("server");

			for (int i = 0; i < servers.getLength(); i++) {
				Element serverElement = (Element) servers.item(i);
				serverSizes.put(serverElement.getAttribute("type"), Integer.valueOf(serverElement.getAttribute("coreCount")));
			}



			while(!serverSizes.isEmpty()) {
				String smallestKey = null;
				int smallestValue = Integer.MAX_VALUE;
				for(String i : serverSizes.keySet()) {
					System.out.println("HERE " + i + " " + serverSizes.get(i));
					if(serverSizes.get(i) < smallestValue) {
						System.out.println("Comparison: " + serverSizes.get(i) + " " +smallestValue);
						smallestKey = i;
						smallestValue = serverSizes.get(i);
					}
				}

				sortedList.put(smallestKey, smallestValue);
				serverSizes.remove(smallestKey);
			} //End of While

			for(String i : sortedList.keySet()) {
				System.out.println("sorted " + i + " " + sortedList.get(i));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return sortedList;
	}


	public serverInfo firstFit(BufferedReader input, PrintWriter out, job jobs, Map<String, Integer> serverSizes) {
		boolean finish = true;
		serverInfo first = null;
		boolean firstFound = false;

		ArrayList<serverInfo> serverList = new ArrayList<serverInfo>();

		try {

			out.println("RESC All");


			String in = "";
			in = input.readLine(); 

			if(in.equals("DATA")) {
				out.println("OK");

				in = input.readLine();  

				while(finish) {


					serverInfo current;

					String[] arrOfStr = in.split(" ");

					current = new serverInfo(arrOfStr[0], arrOfStr[1], arrOfStr[2], arrOfStr[3], arrOfStr[4], arrOfStr[5], arrOfStr[6]);			

					serverList.add(current);

					out.println("OK");


					in = input.readLine();

					if(in.equals(".")) {
						finish = false;
					}
				}

				firstloop:
					for(String i : serverSizes.keySet()) { //For every server type
						if(serverSizes.get(i) >= Integer.valueOf(jobs.cores)) { //If current server type is too small, skip
							for(int j = 0; j < serverList.size(); j++) { //For every server
								serverInfo currServer = serverList.get(j);
								if(currServer.type.equals(i)) { //If current server is of checking server type
									if(Integer.valueOf(currServer.cores) >= Integer.valueOf(jobs.cores)) {
										System.out.println("Current: " + currServer.cores + " " + jobs.cores);
										first = currServer;
										firstFound = true;
										break firstloop;
									}
								}
							}

						}

					}


				outerloop:
					if(!firstFound) { //If no servers found do it again but assign to active servers
						System.out.println("IN");
						for(String i : serverSizes.keySet()) { //For every server type
							System.out.println("List" + serverSizes.get(i) + " " + jobs.cores);
							if(serverSizes.get(i) >= Integer.valueOf(jobs.cores)) {//If current server type is too small, skip
								for(int j = 0; j < serverList.size(); j++) { //For every server
									serverInfo currServer = serverList.get(j);
									if(currServer.type.equals(i)) {
										System.out.println("Not found first time: " + i + " " + jobs.cores);
										first = currServer;
										firstFound = true;
										break outerloop;
									}
								}
							}
						}
					}

			}

		} catch(IOException i) {
			System.out.println(i);
		}

		return first;

	}
	
	public serverInfo cheapestFit(BufferedReader input, PrintWriter out, job jobs, Map<String, Integer> serverSizes) {
		boolean finish = true;
		serverInfo first = null;
		boolean firstFound = false;

		ArrayList<serverInfo> serverList = new ArrayList<serverInfo>();

		try {

			out.println("RESC All");


			String in = "";
			in = input.readLine(); 

			if(in.equals("DATA")) {
				out.println("OK");

				in = input.readLine();  

				while(finish) {


					serverInfo current;

					String[] arrOfStr = in.split(" ");

					current = new serverInfo(arrOfStr[0], arrOfStr[1], arrOfStr[2], arrOfStr[3], arrOfStr[4], arrOfStr[5], arrOfStr[6]);			

					serverList.add(current);

					out.println("OK");


					in = input.readLine();

					if(in.equals(".")) {
						finish = false;
					}
				}

				firstloop:
					for(String i : serverSizes.keySet()) { //For every server type
						if(serverSizes.get(i) >= Integer.valueOf(jobs.cores)) { //If current server type is too small, skip
							for(int j = 0; j < serverList.size(); j++) { //For every server
								serverInfo currServer = serverList.get(j);
								if(currServer.type.equals(i)) { //If current server is of checking server type
									if(Integer.valueOf(currServer.cores) >= Integer.valueOf(jobs.cores)) {
										System.out.println("Current: " + currServer.cores + " " + jobs.cores);
										first = currServer;
										firstFound = true;
										break firstloop;
									}
								}
							}

						}

					}


				outerloop:
					if(!firstFound) { //If no servers found do it again but assign to active servers
						System.out.println("IN");
						for(String i : serverSizes.keySet()) { //For every server type
							System.out.println("List" + serverSizes.get(i) + " " + jobs.cores);
							if(serverSizes.get(i) >= Integer.valueOf(jobs.cores)) {//If current server type is too small, skip
								for(int j = 0; j < serverList.size(); j++) { //For every server
									serverInfo currServer = serverList.get(j);
									if(currServer.type.equals(i)) {
										System.out.println("Not found first time: " + i + " " + jobs.cores);
										first = currServer;
										firstFound = true;
										break outerloop;
									}
								}
							}
						}
					}

			}

		} catch(IOException i) {
			System.out.println(i);
		}

		return first;

	}


	public serverInfo bestFit(BufferedReader input, PrintWriter out, job jobs, Map<String, Integer> serverSizes) { 
		boolean finish = true;
		int largestCpu = 0;
		int bestFit = Integer.MAX_VALUE;
		int minAvail = Integer.MAX_VALUE;
		serverInfo bestFitServer = null;
		int fitVal = 0;
		boolean bestServerFound = false;

		ArrayList<serverInfo> serverList = new ArrayList<serverInfo>();

		try {

			out.println("RESC All");


			String in = "";
			in = input.readLine(); 

			if(in.equals("DATA")) {
				out.println("OK");

				in = input.readLine();  

				while(finish) {

					serverInfo current;

					String[] arrOfStr = in.split(" ");

					for (String a : arrOfStr) {
						System.out.println(a); 
					}

					current = new serverInfo(arrOfStr[0], arrOfStr[1], arrOfStr[2], arrOfStr[3], arrOfStr[4], arrOfStr[5], arrOfStr[6]);

					serverList.add(current);


					out.println("OK");


					in = input.readLine();

					if(in.equals(".")) {
						finish = false;
					}
				}

				for(String i : serverSizes.keySet()) {
					if(serverSizes.get(i) >= Integer.valueOf(jobs.cores)) { 
						for(int j = 0; j < serverList.size(); j++) { 
							serverInfo currServer = serverList.get(j);
							if(currServer.type.equals(i)) { 
								if(!Integer.valueOf(currServer.cores).equals(0)) {
									System.out.println("Current cores: " + currServer.cores);
									if(Integer.valueOf(currServer.cores) >= Integer.valueOf(jobs.cores) && Integer.valueOf(currServer.memory) >= Integer.valueOf(jobs.memory) && Integer.valueOf(currServer.diskSpace) >= Integer.valueOf(jobs.disk)) {
										fitVal = Integer.valueOf(jobs.cores) - Integer.valueOf(currServer.cores);
										if(Math.abs(fitVal) < bestFit || (Math.abs(fitVal) == bestFit && Integer.valueOf(currServer.time) < minAvail)) {
											System.out.println("TIme diff " + currServer.time + " " + minAvail);
											bestFit = Math.abs(fitVal);
											minAvail = Integer.valueOf(currServer.time);
											System.out.println("Set as" + fitVal + " " + currServer.type + currServer.id);
											System.out.println("TIme conc "+ minAvail);

											bestFitServer = currServer;
											System.out.println(bestFitServer);
											bestServerFound = true;
										}
									}
								}
							}
						}
					}
				}

				ServerFound:
					if(!bestServerFound) { //If no servers found do it again but assign to active servers
						System.out.println("IN");
						for(String i : serverSizes.keySet()) { //For every server type
							System.out.println("List" + serverSizes.get(i) + " " + jobs.cores);
							if(serverSizes.get(i) >= Integer.valueOf(jobs.cores)) {//If current server type is too small, skip
								for(int j = 0; j < serverList.size(); j++) { //For every server
									serverInfo currServer = serverList.get(j);
									if(currServer.type.equals(i)) {
										System.out.println("Not found first time: " + i + " " + jobs.cores);
										bestFitServer = currServer;
										bestServerFound = true;
										break ServerFound;
									}
								}
							}
						}
					}

			}

		} catch(IOException i) {
			System.out.println(i);
		}


		return bestFitServer;

	}


	public serverInfo findLargest(BufferedReader input, PrintWriter out, job jobs) { 
		boolean finish = true;
		int largestCpu = 0;
		serverInfo largest = null;

		try {

			out.println("RESC All");


			String in = "";
			in = input.readLine(); 

			if(in.equals("DATA")) {
				out.println("OK");

				in = input.readLine();  

				while(finish) {
					serverInfo current;

					String[] arrOfStr = in.split(" ");

					for (String a : arrOfStr) {
						System.out.println(a); 
					}

					current = new serverInfo(arrOfStr[0], arrOfStr[1], arrOfStr[2], arrOfStr[3], arrOfStr[4], arrOfStr[5], arrOfStr[6]);


					int cpuCount = 0;

					cpuCount = Integer.valueOf(current.cores);

					if(cpuCount > largestCpu) {
						largest = current;
						largestCpu = cpuCount;
					}

					out.println("OK");


					in = input.readLine();

					if(in.equals(".")) {
						finish = false;
					}
				}
			}

		} catch(IOException i) {
			System.out.println(i);
		}


		return largest;

	}
	public serverInfo worstFit(BufferedReader input, PrintWriter out, job jobs, Map<String, Integer> serverSizes) { 
		boolean finish = true;
		int largestCpu = 0;
		int worstFit = Integer.MIN_VALUE;
		int altFit = Integer.MIN_VALUE;
		serverInfo worstFitServer = null;
		serverInfo altFitServer = null;
		boolean worstServerFound = false;
		boolean altFitServerFound = false;
		int fitnessValue = 0;

		ArrayList<serverInfo> serverList = new ArrayList<serverInfo>();

		try {

			out.println("RESC All");


			String in = "";
			in = input.readLine(); 

			if(in.equals("DATA")) {
				out.println("OK");

				in = input.readLine();  

				while(finish) {

					serverInfo current;

					String[] arrOfStr = in.split(" ");

					for (String a : arrOfStr) {
						System.out.println(a); 
					}

					current = new serverInfo(arrOfStr[0], arrOfStr[1], arrOfStr[2], arrOfStr[3], arrOfStr[4], arrOfStr[5], arrOfStr[6]);

					serverList.add(current);

					out.println("OK");

					in = input.readLine();

					if(in.equals(".")) {
						finish = false;
					}
				}

				for(String i : serverSizes.keySet()) {
					if(serverSizes.get(i) >= Integer.valueOf(jobs.cores)) { 
						for(int j = 0; j < serverList.size(); j++) { 
							serverInfo currServer = serverList.get(j);
							if(currServer.type.equals(i)) { 
								if(!Integer.valueOf(currServer.cores).equals(0)) {
									System.out.println("Current cores: " + currServer.cores);
									if(Integer.valueOf(currServer.cores) >= Integer.valueOf(jobs.cores) && Integer.valueOf(currServer.memory) >= Integer.valueOf(jobs.memory) && Integer.valueOf(currServer.diskSpace) >= Integer.valueOf(jobs.disk)) {
										fitnessValue = Integer.valueOf(jobs.cores) - Integer.valueOf(currServer.cores);
										if(Math.abs(fitnessValue) > worstFit && Integer.valueOf(currServer.time) == -1){
											worstFit = Math.abs(fitnessValue);
											worstFitServer = currServer;
											System.out.println(worstFitServer);
											worstServerFound = true;
										}
										else if(Math.abs(fitnessValue) > altFit && Integer.valueOf(currServer.state) < 3){
											altFit = Math.abs(fitnessValue);
											altFitServer = currServer;
											System.out.println(altFitServer);
											altFitServerFound = true;
										}
									}
								}
							}
						}
					}
				}

				ServerFound:
					if(!worstServerFound && !altFitServerFound) { //If no servers found do it again but assign to active servers
						System.out.println("IN");
						for(String i : serverSizes.keySet()) { //For every server type
							System.out.println("List" + serverSizes.get(i) + " " + jobs.cores);
							if(serverSizes.get(i) >= Integer.valueOf(jobs.cores)) {//If current server type is too small, skip
								for(int j = 0; j < serverList.size(); j++) { //For every server
									serverInfo currServer = serverList.get(j);
									if(currServer.type.equals(i)) {
										System.out.println("Not found first time: " + i + " " + jobs.cores);
										worstFitServer = currServer;
										worstServerFound = true;
										break ServerFound;
									}
								}
							}
						}
					}

			}

		} catch(IOException i) {
			System.out.println(i);
		}

		if(worstServerFound == true) {
			return worstFitServer;
		}
		return altFitServer;

	}

	public void schedule(BufferedReader input, PrintWriter out, job jobInfo, serverInfo server) { 
		String temp = ("SCHD " + jobInfo.id + " " + server.type + " " + server.id);
		String in = "";
		try {
			out.println(temp);

			in = input.readLine();
			System.out.println(in);

		} catch(IOException i) {
			System.out.println(i);

		}

	}

	public job jobInfo(String in) { 

		String[] strArr = in.split(" ");

		job jobs = new job(strArr[1], strArr[2], strArr[3], strArr[4], strArr[5], strArr[6]);


		return jobs;

	} 
} 